import { Options, ParamsOpt } from './models/params';

interface StoreOptions {
  url: string;
  storeUTMs: boolean; // Store UTM params
  include: string[]; // List of custom URL params to be stored.
  exclude: string[]; // Excluded URL params
}

export class StoreUrlParams {
  private options: StoreOptions = {
    url: location.href,
    storeUTMs: true,
    include: [],
    exclude: [],
  };

  private storage: Storage;
  private location: URL;

  constructor(storage: Storage = localStorage, options?: Options) {
    this.storage = storage;
    // User defined options (might have more in the future)
    if (options) {
      Object.keys(options).forEach((key) => {
        (this.options as any)[key] = (options as any)[key];
      });
    }

    this.location = new URL((this.options.url as string) || location.href);

    if (this.options.storeUTMs) {
      this.options.include = [
        ...this.options.include,
        ...['utm_medium', 'utm_campaign', 'utm_term', 'utm_content'],
      ];
    }
  }

  getParams = (): ParamsOpt => {
    const searchParams = this.location.searchParams;
    searchParams.sort();
    const include = this.options.include.filter((param: string) =>
      searchParams.has(param),
    );
    const params: any = include.map((itm: string) => {
      const value = searchParams.get(itm) || '';
      return { key: value };
    });
    console.log(params);
    return params as ParamsOpt;
  };
}
