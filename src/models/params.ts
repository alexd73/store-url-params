export type Options = Record<string, undefined | string | number | boolean | Array<any>>;

export type ParamsOpt = Record<string, string>;
