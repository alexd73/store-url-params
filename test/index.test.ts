import test from 'ava';
import { StoreUrlParams, ParamsOpt } from '../src/';

const storeParams = new StoreUrlParams(localStorage, {
  url:
    'https://www.example.com/?utm_source=summer-mailer&utm_medium=email&utm_campaign=summer-sale',
});

test('fn() returns foo', (t: { is: (arg0: any, arg1: string) => void }) => {
  t.is(storeParams.getParams(), 'foo');
});
