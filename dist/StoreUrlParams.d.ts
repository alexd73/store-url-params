import { Options, ParamsOpt } from './models/params';
export declare class StoreUrlParams {
    private options;
    private storage;
    private location;
    constructor(storage?: Storage, options?: Options);
    getParams: () => ParamsOpt;
}
