"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StoreUrlParams = void 0;
class StoreUrlParams {
    constructor(storage = localStorage, options) {
        this.options = {
            url: location.href,
            storeUTMs: true,
            include: [],
            exclude: [],
        };
        this.getParams = () => {
            const searchParams = this.location.searchParams;
            searchParams.sort();
            const include = this.options.include.filter((param) => searchParams.has(param));
            const params = include.map((itm) => {
                const value = searchParams.get(itm) || '';
                return { key: value };
            });
            console.log(params);
            return params;
        };
        this.storage = storage;
        // User defined options (might have more in the future)
        if (options) {
            Object.keys(options).forEach((key) => {
                this.options[key] = options[key];
            });
        }
        this.location = new URL(this.options.url || location.href);
        if (this.options.storeUTMs) {
            this.options.include = [
                ...this.options.include,
                ...['utm_medium', 'utm_campaign', 'utm_term', 'utm_content'],
            ];
        }
    }
}
exports.StoreUrlParams = StoreUrlParams;
//# sourceMappingURL=StoreUrlParams.js.map