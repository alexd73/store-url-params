export declare type Options = Record<string, undefined | string | number | boolean | Array<any>>;
export declare type ParamsOpt = Record<string, string>;
